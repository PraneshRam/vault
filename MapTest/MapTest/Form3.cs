﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Resources;
using System.Net.Mail;
using System.Threading;
using GMap.NET;
using System.Net;

namespace MapTest
{
    public partial class Form3 : Form
    {
        string server;
        string database;
        string uid;
        string password;
        MySqlConnection connection;
        string filename;
        string pathname;
        string connectionString;
        static int load=0;
        Thread t;
        public Form3()
        {
            if (load == 0)
            {
                t = new Thread(new ThreadStart(splashscreen));
                t.Start();
                Thread.Sleep(5000);
            }
            InitializeComponent();
            if (load == 0)
            {
                t.Abort();
                load = 1;
            }
            server = "localhost";
            database = "vault";
            uid = "root";
            password = "";
            //string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

            connection = new MySqlConnection(connectionString);
            ToolTip ttp1 = new ToolTip();
            ttp1.SetToolTip(button1, "Add File to List");
            ToolTip ttp2 = new ToolTip();
            ttp1.SetToolTip(button2, "Delete File from List");
            ToolTip ttp3 = new ToolTip();
            ttp1.SetToolTip(button6, "Hide File List");
            ToolTip ttp4 = new ToolTip();
            ttp1.SetToolTip(button7, "Unhide File List");
            ToolTip ttp5 = new ToolTip();
            ttp1.SetToolTip(button5, "Change Password");
            //Console.WriteLine(Properties.Settings.Default.Pin);
        }
        public void splashscreen()
        {
            Application.Run(new Form6());
        }
        private void button1_Click(object sender, EventArgs e)
        {
            int flag = 1;
            Form1 addFile = new Form1(3);
            addFile.ShowDialog();
            if (addFile.passCheck())
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();

                openFileDialog1.InitialDirectory = @"C:\";
                openFileDialog1.Title = "Browse Text Files";

                openFileDialog1.CheckFileExists = true;
                openFileDialog1.CheckPathExists = true;

                openFileDialog1.DefaultExt = "txt";
                openFileDialog1.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog1.FilterIndex = 2;
                openFileDialog1.RestoreDirectory = true;

                openFileDialog1.ReadOnlyChecked = true;
                openFileDialog1.ShowReadOnly = true;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string query = "select path from files";
                    connection.Open();
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    MySqlDataReader reader;

                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        if (reader.GetString(0).Equals(openFileDialog1.FileName))
                        {
                            flag = 0;
                        }
                    }
                    connection.Close();
                    filename = openFileDialog1.SafeFileName;
                    pathname = (openFileDialog1.FileName).Replace(@"\", @"\\");
                }
                else
                {
                    Console.WriteLine("error getting filename");
                }
                DialogResult confirm = MessageBox.Show("Confirm add file to hide list?", "Confirmation", MessageBoxButtons.YesNo);
                if (confirm == DialogResult.Yes && flag == 1)
                {
                    string query = "insert into files(name,path) values('" + filename + "','" + pathname + "')";
                    connection.Open();
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
                else if (flag == 0)
                {
                    DialogResult fileexits = MessageBox.Show("File Already in the list.");
                }
                else
                {

                }
            }
            else
            {
                DialogResult wrongpass = MessageBox.Show("Wrong Password.Please Retry");
            }

        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {

            //Properties.Settings.Default.Password = "fff";
            //Properties.Settings.Default.Save();
            Console.WriteLine(Properties.Settings.Default.Password);
            Form1 checkPass = new MapTest.Form1(1);
            checkPass.ShowDialog();
            //checkPass.SetPass();
            //Console.WriteLine(checkPass.passCheck());
            if (checkPass.passCheck())
            {
                Form1 setPass = new Form1(2);

                setPass.ShowDialog();
                setPass.SetPass();
            }


            //Console.WriteLine("hi");

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form1 checkPass = new Form1(3);
            checkPass.ShowDialog();
            Console.WriteLine(checkPass.passCheck());
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form1 a = new Form1();
            a.ShowDialog();
            a.SetPass();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Form1 lockFile = new Form1(3);
            lockFile.ShowDialog();
            if (lockFile.passCheck())
            {
                DialogResult confirm = MessageBox.Show("Confirm Lock the list?", "Confirmation", MessageBoxButtons.YesNo);
                if (confirm == DialogResult.Yes)
                {
                    string query = "select * from files";
                    connection.Open();
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    MySqlDataReader reader;
                    reader = cmd.ExecuteReader();
                    string sourcepath;
                    string destpath;
                    int hideflag = 0;
                    string destfolder = @"c:\vault";
                    if (!System.IO.Directory.Exists(destfolder))
                    {
                        System.IO.Directory.CreateDirectory(destfolder);


                    }
                    while (reader.Read())
                    {
                        if (reader.GetInt16(3) == 0)
                        {
                            sourcepath = reader.GetString(2);
                            destpath = destfolder + @"\" + reader.GetString(1);
                            try
                            {
                                System.IO.File.Move(sourcepath, destpath);
                            }
                            catch
                            {
                                DialogResult confirmhidefail = MessageBox.Show("Hide Failure", "Confirmation", MessageBoxButtons.OK);
                                hideflag = 1;

                            }
                            if (hideflag == 0)
                            {

                                query = "update files set status=1 where id=" + reader.GetInt16(0);
                                MySqlConnection stat = new MySqlConnection(connectionString);
                                MySqlCommand status = new MySqlCommand(query, stat);
                                stat.Open();
                                status.ExecuteNonQuery();
                                stat.Close();
                            }
                        }

                    }
                    if (hideflag == 0)
                    {
                        DialogResult confirmhide = MessageBox.Show("Hide Successful", "Confirmation", MessageBoxButtons.OK);
                    }
                    connection.Close();
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Form1 unlockFile = new Form1(3);
            unlockFile.ShowDialog();
            if (unlockFile.passCheck())
            {
                DialogResult confirm= MessageBox.Show("Confirm to Unlock the list?", "Confirmation", MessageBoxButtons.YesNo);
                if (confirm == DialogResult.Yes)
                {
                    string query = "select * from files";
                    connection.Open();
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    MySqlDataReader reader;
                    reader = cmd.ExecuteReader();
                    string sourcepath;
                    int unlockflag = 0;
                    string destpath;
                    //string destfolder = @"c:\vault";
                    while (reader.Read())
                    {
                        if (reader.GetInt16(3) == 1)
                        {
                            sourcepath = @"c:\vault\" + reader.GetString(1);
                            destpath = reader.GetString(2);
                            try
                            {
                                System.IO.File.Move(sourcepath, destpath);
                            }
                            catch
                            {
                                DialogResult unlockfail = MessageBox.Show("Unlock Failure", "Confirmation", MessageBoxButtons.OK);
                                unlockflag = 1;
                            }
                            if (unlockflag == 0)
                            {
                                query = "update files set status=0 where id=" + reader.GetInt16(0);
                                MySqlConnection stat = new MySqlConnection(connectionString);
                                MySqlCommand status = new MySqlCommand(query, stat);
                                stat.Open();
                                status.ExecuteNonQuery();
                                stat.Close();
                            }
                        }

                    }
                    if (unlockflag == 0)
                    {
                        DialogResult confirmUnlock = MessageBox.Show("Unlock Successful", "Confirmation", MessageBoxButtons.OK);
                    }
                }
                connection.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

            Form1 delfromList = new Form1(3);
            delfromList.ShowDialog();
            if (delfromList.passCheck())
            {
                DeleteList del = new DeleteList();
                del.ShowDialog();
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Form4 pin = new Form4();
            pin.ShowDialog();
            if (pin.checkPin())
            {
                Form1 setPass = new Form1(2);

                setPass.ShowDialog();
                setPass.SetPass();
            }else
            {
               /* MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                mail.From = new MailAddress("praneshvrp@gmail.com");
                mail.To.Add("praneshvrp@gmail.com");
                mail.Subject = "Test Mail";
                mail.Body = "This is for testing SMTP mail from GMAIL";

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("praneshvrp@gmail.com", "vrprtcoevrmala");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                MessageBox.Show("mail Send");
                */
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

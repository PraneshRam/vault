﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace MapTest
{
    public partial class DeleteList : Form
    {
        string server;
        string database;
        string uid;
        string password;
        MySqlConnection connection;
        public DeleteList()
        {
            InitializeComponent();
            server = "localhost";
            database = "vault";
            uid = "root";
            password = "";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
            connection = new MySqlConnection(connectionString);
            this.gridSetup();
        }
        public void gridSetup()
        {
            connection.Open();
            MySqlDataAdapter dAdapter = new MySqlDataAdapter("select * from files",connection);
            DataTable dt = new DataTable();
            dAdapter.Fill(dt);
            DataGridViewCheckBoxColumn chk = new DataGridViewCheckBoxColumn();
            dataGridView1.Columns.Add(chk);
            chk.HeaderText = "Delete";
            chk.Name = "chk";
            dataGridView1.DataSource = dt;
            connection.Close();

        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Delete_Click(object sender, EventArgs e)
        {
            List<DataGridViewRow> selectedRows = (from row in dataGridView1.Rows.Cast<DataGridViewRow>()
                                                  where Convert.ToBoolean(row.Cells["chk"].Value) == true
                                                  select row).ToList();
            //Console.WriteLine(selectedRows[0].Cells["name"].Value);
            string sourcepath;
            string destpath;
            string sourcefolder= @"c:\vault"; 
            foreach (DataGridViewRow row in selectedRows)
            {
                if (Convert.ToInt16(row.Cells["status"].Value)==1)
                {
                    sourcepath = sourcefolder + @"\" + row.Cells["name"].Value;
                    destpath = Convert.ToString(row.Cells["path"].Value);
                    connection.Open();

                    try
                    {
                        System.IO.File.Move(sourcepath, destpath);
                    }
                    catch
                    {

                    }
                    string query = "delete from files where id=" + Convert.ToInt16(row.Cells["id"].Value) + "";
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }else
                {
                    connection.Open();

                    string query = "delete from files where id=" + Convert.ToInt16(row.Cells["id"].Value) + "";
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
            this.gridSetup();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form3 back = new Form3();
            back.Show();
            this.Close();
        }
    }
}
